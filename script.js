const $ = document
const url = "https://treinamento-ajax-api.herokuapp.com"
const author = "Pedro Paradela"

const idAtual = 373

getMessages()

// Deixando o "Digite a mensagem..."
resetInput()


// Evento que faz o "Digite a mensagem" sumir
const msg = $.querySelector(".input-msg")
msg.addEventListener("click", (event) => {
    if (msg.value === "Digite a mensagem...") {
        msg.style.color = "black"
        msg.value = ""
    }
})


// Evento para enviar com o enter:
msg.addEventListener("keydown", (event) => {
    if (event.keyCode === 13) {
        event.preventDefault(true)
        send()
    }
})


// Criando o botão Enviar
const localBotao = $.querySelector(".entrada")
localBotao.appendChild(createButton("Enviar", "btn-enviar", "#0066CC", "#73b9ff", "white", button => {

    // Clicar no botão
    button.addEventListener("click", event => {
        send()
        resetInput()
    })
}, "10px 20px"))


// Função que cria os botões de excluir e editar:
function createEdEx(div) {
    div.appendChild(createButton("Editar", "btn-ed", "#F6A75D", "#ffc894", "black", button => button.addEventListener("click", event => {
        const parent = button.parentElement
        const button2 = parent.lastChild

        button.remove()
        button2.remove()

        inputEditar = $.createElement("input")
        inputEditar.classList.add("input-editar")
        inputEditar.addEventListener("keydown", event => {
            if (event.keyCode === 13) {
                event.preventDefault(true)
                save()
            }
        })


        parent.appendChild(inputEditar)
        parent.appendChild(createButton("Salvar", "btn-salvar", "rgb(44, 202, 44)", "rgb(164, 255, 164)", "white", button => button.addEventListener("click", event => save())))
    })))
    div.appendChild(createButton("Excluir", "btn-ex", "#F65D5D", "#ff8989", "black", button => button.addEventListener("click", event => deletar(button, idAtual))))
}


// Função que envia a mensagem
function addHistorico(valores) {
    const mensagem = $.createElement("li")

    mensagem.id = valores[2]

    let input = valores[1]
    // Criando a mensagem
    mensagem.innerHTML = `
    <div class="autor">
        <h2>${valores[0]}</h2>
    </div>
    <p class="texto">${input}</p>
        <div class="botoes">
        </div>`

    // Criando os botões
    const div = mensagem.lastChild
    createEdEx(div)

    const ul = $.querySelector(".historico")
    ul.appendChild(mensagem)
}

function send() {
    const input = $.querySelector(".input-msg")

    if (input.value != "") {

        addHistorico([author, input.value, 0])
        postMessage(input.value)

    }

}


// Função de editar
function save() {
    const input = $.querySelector(".input-editar")
    const parent = input.parentElement
    const mensagem = input.parentElement.parentElement.firstChild

    mensagem.innerText = input.value
    updateMessage(input.value, idAtual)

    parent.removeChild(parent.lastChild)

    // Criando os botões
    const div = parent
    createEdEx(div)


    parent.removeChild(input)
}


// Função de criar botão
function createButton(text = "Enviar", classe = text, background = "white", backgroundHover = "white", color = "black", callback, padding = "5px") {
    const botao = $.createElement('button')
    botao.innerText = text
    botao.classList.add(classe)

    botao.style.cssText = `
        background-color: ${background};
        color: ${color};
        padding: ${padding};
    `

    botao.addEventListener("mouseover", event => event.target.style.background = backgroundHover)

    botao.addEventListener("mouseout", event => event.target.style.background = background)

    try {
        callback(botao)
    } catch (err) {

    }

    return botao
}

function deletar(elemento) {
    const parent = elemento.parentElement.parentElement
    deleteMessage(parent.id)
    parent.remove();
}

function resetInput() {
    // Digite a mensagem
    const msg = $.querySelector(".input-msg")
    msg.value = "Digite a mensagem..."
    msg.style.color = "#969696"
}


// Aula Ajax



function getMessages() {

    let arrayMensagens = []
    // Get para buscar as mensagens
    fetch(url + "/messages")
        .then(message => message.json())
        .then(mensagemTextos => {
            for (mensagem of mensagemTextos) {
                addHistorico([mensagem["author"], mensagem["content"], mensagem["id"]])
            }
        })
        .catch(error => console.error(error))
        .finally(() => {
            console.log("Requisição Finalizada")
        })

}

const getMessage = (id) => {
    // Get para buscar mensagem específica pelo ID
    fetch(url + "/messages/" + id)
        .then(mensagem => mensagem.json())
        .then(mensagemTexto => console.log(mensagemTexto))
        .catch(error => console.error(error))
        .finally(() => console.log("Requisição Finalizada"))
}

const postMessage = (texto) => {
    // Criar uma nova mensagem na API
    const body = {
        message: {
            content: texto,
            author: author
        }
    }

    const config = {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        }
    }

    fetch(url + "/messages", config)
        .then(mensagem => mensagem.json())
        .then(mensagemTexto => {
            console.log(mensagemTexto)
        })
        .catch(error => console.error(error))
        .finally(() => console.log("Requisição Finalizada"))
}

const updateMessage = (mensagem, id) => {
    //  Update em uma mensagem específica
    const body = {
        message: {
            content: mensagem,
        }
    }

    const config = {
        method: "PUT",
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        }
    }

    fetch(url + "/messages/" + id, config)
        .then(mensagem => mensagem.json())
        .then(mensagemTexto => {
            console.log(mensagemTexto)
        })
        .catch(error => console.error(error))
        .finally(() => console.log("Requisição Finalizada"))
}

const deleteMessage = (id) => {
    // Deletando mensagem especifica
    const config = {
        method: "DELETE"
    }

    fetch(url + "/messages/" + id, config)
        .catch(error => console.error(error))
        .finally(() => console.log("Requisição Finalizada"))
}

const btnRefreshLocal = $.querySelector(".refresh")
btnRefreshLocal.appendChild(createButton("Refresh", "btn-rfrsh", "#00d9ff", "#6ce9ff", "white", button => {
    button.addEventListener('click', () => {
        const hist = $.querySelector("ul")
        const filhos = hist.children
        let tam = filhos.length

        for (let i = 0; i < tam; i++) {
            hist.removeChild(hist.firstChild)
        }
        try {
            hist.removeChild(hist.firstChild)
        }
        catch (e) {

        }

    })
    button.addEventListener('click', () => {
        getMessages()
    })
}))

